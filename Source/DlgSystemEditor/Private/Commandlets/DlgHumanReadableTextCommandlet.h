// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Commandlets/Commandlet.h"

#include "DlgHumanReadableTextCommandlet.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogDlgHumanReadableTextCommandlet, All, All);

class UDlgDialogue;
class UDlgNode;
class UDialogueGraphNode;

USTRUCT()
struct FDlgNodeContext_FormatHumanReadable
{
	GENERATED_USTRUCT_BODY()
public:
	// Some context
	UPROPERTY()
	FName Speaker;

	UPROPERTY()
	TArray<int32> ParentNodeIndices;

	UPROPERTY()
	TArray<int32> ChildNodeIndices;
};


// Variant of the FDlgEdge that also tells us from what node to what node
USTRUCT()
struct FDlgEdgeOrphan_FormatHumanReadable
{
	GENERATED_USTRUCT_BODY()

public:
	// Metadata
	UPROPERTY()
	int32 SourceNodeIndex = INDEX_NONE;

	UPROPERTY()
	int32 TargetNodeIndex = INDEX_NONE;

	UPROPERTY()
	FText Text;
};


// Variant of the FDlgEdge that is human readable
USTRUCT()
struct FDlgEdge_FormatHumanReadable
{
	GENERATED_USTRUCT_BODY()

public:
	// Metadata
	UPROPERTY()
	int32 TargetNodeIndex = INDEX_NONE;

	UPROPERTY()
	FText Text;
};


// Variant of the FDlgSpeechSequenceEntry that is human readable
USTRUCT()
struct FDlgSpeechSequenceEntry_FormatHumanReadable
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY()
	FText Text;

	UPROPERTY()
	FText EdgeText;
};


// Variant of the UDlgNode_SpeechSequence that is human readable
USTRUCT()
struct FDlgNodeSpeechSequence_FormatHumanReadable
{
	GENERATED_USTRUCT_BODY()

public:
	// Metadata
	UPROPERTY()
	int32 NodeIndex = INDEX_NONE;

	UPROPERTY()
	TArray<FDlgSpeechSequenceEntry_FormatHumanReadable> Sequence;

	UPROPERTY()
	TArray<FDlgEdge_FormatHumanReadable> Edges;
};


// Variant of the UDlgNode_Speech that is human readable
USTRUCT()
struct FDlgNodeSpeech_FormatHumanReadable
{
	GENERATED_USTRUCT_BODY()

public:
	// Metadata, NodeIndex
	UPROPERTY()
	int32 NodeIndex = INDEX_NONE;

	UPROPERTY()
	FText Text;

	UPROPERTY()
	TArray<FDlgEdge_FormatHumanReadable> Edges;
};


// Variant of the UDlgDialogue that is human readable
USTRUCT()
struct FDlgDialogue_FormatHumanReadable
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY()
	FName DialogueName;

	UPROPERTY()
	FGuid DialogueGuid;

	UPROPERTY()
	TArray<FDlgNodeSpeech_FormatHumanReadable> SpeechNodes;

	UPROPERTY()
	TArray<FDlgNodeSpeechSequence_FormatHumanReadable> SpeechSequenceNodes;
};


UCLASS()
class UDlgHumanReadableTextCommandlet : public UCommandlet
{
	GENERATED_BODY()

public:
	UDlgHumanReadableTextCommandlet();

public:

	//~ UCommandlet interface
	int32 Main(const FString& Params) override;

	static bool ExportDialogueToHumanReadableFormat(const UDlgDialogue& Dialogue, FDlgDialogue_FormatHumanReadable& OutFormat);
	static bool ExportNodeToContext(const UDlgNode* Node, FDlgNodeContext_FormatHumanReadable& OutContext);

	static bool ImportHumanReadableFormatIntoDialogue(const FDlgDialogue_FormatHumanReadable& Format, UDlgDialogue* Dialogue);

	// Tells us if the edge text is default
	static bool IsEdgeTextDefault(const FText& EdgeText);

	static bool SaveAllDirtyDialogues();
	static bool SaveAllDialogues();

protected:
	// Own methods
	int32 Export();
	int32 Import();

	static bool SetGraphNodesNewEdgesText(UDialogueGraphNode* GraphNode, const TArray<FDlgEdge_FormatHumanReadable>& Edges, const int32 NodeIndex, const UDlgDialogue* Dialogue);

protected:
	FString OutputInputDirectory;

	TArray<UPackage*> PackagesToSave;

	static const TCHAR* FileExtension;
};
